package com.epam;

import fibonacci.FibonacciGenerator;
import fibonacci.FibonacciOperation;


/**
 * Main class for start point of program
 */
public class Application {
    /**
     * Here is the start point of the program
     *
     * @param args comand line values
     */
    public static void main(String[] args) {
        int[] array;
        FibonacciGenerator fg = new FibonacciGenerator();
        FibonacciOperation fo = new FibonacciOperation();
        array = fg.generateFibonacci(12);

        fo.printEvenSum(array);
        fo.printOddSum(array);
        fo.printOdd(array);
        System.out.println();
        fo.printInvEven(array);
        System.out.println();
        fo.writePercentageEven(array);
        fo.writPercentageOdd(array);

        System.out.println("s");
    }
}
