package fibonacci;

/**
 * Class for methods than do operations for task of Fibonacci sequence (print inverse even numbers,
 * print only odd numbers, print sum of event/odd numbers, write  the ratio of  even numbers
 * to odd and vice versa in percent)
 *
 *
 */
public class FibonacciOperation {
    /**
     * Variable nead for sum methods as a starter sum
     */
    private int sum;

    public FibonacciOperation() {
        this.sum = 0;
    }
    /**
     * Performs the checking of  odd numbers and print result in console
     * @param sequence Accept int array of Fibonacci sequence
     */
    public void printOdd(int... sequence) {

        for (int i = 0; i < sequence.length; i++) {
            if (sequence[i] % 2 == 0) {
            } else {
                System.out.print(sequence[i] + " ");
            }
        }
    }
    /**
     * Performs the checking of  even numbers  print result in console in the opposite direction
     * @param sequence Accept int array of Fibonacci sequence
     */
    public void printInvEven(int... sequence) {

        for (int i = sequence.length - 1; i >= 0; i--) {
            if (sequence[i] % 2 == 0) {
                System.out.print(sequence[i] + " ");
            }
        }

    }
    /**
     * Printing sum of even numbers of sequence
     * @param sequence Accept int array of Fibonacci sequence
     */
    public void printEvenSum(int... sequence) {
        for (int i = 0; i < sequence.length; i++) {
            if (sequence[i] % 2 == 0) {
                sum += sequence[i];
            }
        }
        System.out.println("The sum of even numbers is " + sum);
    }
    /**
     * Printing sum of odd numbers of sequence
     * @param sequence Accept int array of Fibonacci sequence
     */
    public void printOddSum(int... sequence) {
        for (int i = 0; i < sequence.length; i++) {
            if (sequence[i] % 2 != 0) {
                sum += sequence[i];
            }
        }
        System.out.println("The sum of odd numbers is " + sum);
    }
    /**
     * Writes the ratio of even numbers  to odd  in percent
     * @param sequence Accept int array of Fibonacci sequence
     */
    public void writePercentageEven(int... sequence) {
        double even;
        even = 100 - calcPersentOdd(sequence);
        System.out.println("Percentage of even: " + even + "%");
    }
    /**
     * Writes the ratio of odd numbers to even  in percent
     * @param sequence Accept int array of Fibonacci sequence
     */
    public void writPercentageOdd(int[] sequence) {
        System.out.println("Percentage of odd: " + calcPersentOdd(sequence) + "%");
    }

    /**
     * Culculates the ratio of odd numbers to even in percent
     * @param array Accept int array of Fibonacci sequence
     * @return Persentge
     */
    private double calcPersentOdd(int... array) {
        double odd = 0;
        double even = 0;
        double percent;

        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 != 0) {
                odd++;
            } else {
                even++;
            }
        }

        percent = odd * 100 / (odd + even);
        return percent;
    }

}